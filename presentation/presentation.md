---
title:
- ICSA et SAV+R <br> an 3
author:
- Nicolas Bouillot
institute:
- Société des arts technologiques
theme:
- moon
date:
- 12 Juillet 2022
aspectratio:
- 169
navigation:
- empty
section-titles:
- false
output:
  revealjs::revealjs_presentation:
    css: styles.css
---


# Structure de la présentation

* Compte-rendu ICSA
* Compte-rendu SAV+R

Le projet ICSA
====
Immersion Collective Spontanée et Adaptative

![hacklab  -image-no-border](./img/hacklab-rond.png){width=20%}
![livepose -image-no-border](./img/livepose-rond.png){width=20%}
![mirador -image-no-border](./img/mirador-rond.png){width=20%}

# Concepts-clefs

* Calibrage
* Équiper les lieux plutôt que les individus
* Édition colaborative/interactivitée

# 1 --- Calibrage des dispositifs d'affichage

![calib -image-no-border](./img/ICSA-prob-calib.jpg){width=60%}

Comment automatiser le calibrage des vidéoprojecteurs et des systèmes audio spatialisés ?

# Avancement — Calibrage

![calibui  -image-no-border](./img/splash-calib-ui.jpg){width=40%}
![calibcol -image-no-border](./img/splash-color-calib.jpg){width=40%}

Les principaux travaux d'amélioration de Splash : conception d'une interface de calibrage et calibrage colorimétrique 

# 2 --- Déploiement d'espaces télé-immersifs

![depl -image-no-border](./img/hacklab_SebastienRoy2.jpg){width=60%}

Comment faciliter la mise en œuvre d'espaces télé-immersifs temporaires ?

# Avancement — Déploiement

<iframe width="560" height="315" src="https://www.youtube.com/embed/hHy8_tm9nwU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# 3 --- Édition collaborative in situ

![edit -image-no-border](./img/icsa_livepose_molecule.jpg){width=60%}

Comment permettre à de multiples utilisateurs d'interagir sans contrôleur physique ?

# Avancement — Édition collaborative

![calibui  -image-no-border](./img/hacklab_SebastienRoy.jpg){width=40%}
![calibcol -image-no-border](./img/molecule.jpg){width=40%}

Suivi des personnes et détection de gestes.

# Partenariats

|                  | Splash | SATIE | LivePose | Switcher | Audiodice | Mirador |
|------------------|:------:|:-----:|:--------:|:--------:|:---------:|:-------:|
| Mitacs A. Doualo |   x    |   x   |          |          |     x     |    x    |
| Créo             |   x    |       |    x     |          |           |    x    |
| EnsAD            |   x    |   x   |    x     |          |     x     |    x    |
| Motus Domum      |        |   x   |    x     |    x     |     x     |         |
| MusicMotion      |   x    |   x   |    x     |          |     x     |    x    |
| Ossia            |   x    |   x   |    x     |    x     |     x     |    x    |
| Percepto         |        |       |    x     |          |           |         |
| Résidences R&D   |   x    |   x   |    x     |          |     x     |    x    |
| Seeker           |        |   x   |          |          |           |         |

Résultats sur 3 ans
========

* ✅ nouvelles expériences immersives dans des lieux non dédiés
* ✅ amélioration des méthodes de calibrage audio et vidéo
* ✅ amélioration des techniques de projection sur des objets mobiles suivis, incluant l’interactivité par détection de squelette
* ✅ facilitation du déploiement d’espaces immersifs
* ✅ facilitation de la création de contenu pour ces nouveaux espaces immersifs
* ✅ Nouvelles approches et méthodes de travail pour l'édition de contenu in situ;

Partenaires sur 3 ans
=======

* Industriels (4) : _CAE, Creo, Centech, Mozilla 🇺🇸_
* Diffuseurs (5) : _MusicMotion, ossia 🇫🇷, Eastern Block, ENC, Centro Cultura Digital 🇲🇽_
* Académiques (8) : _MITACS/Audrey, EnsAD 🇫🇷, Tech3Lab, CNRS/Pardoen 🇫🇷, ETS/numerix, CHUM, Stanford 🇺🇸, D. Brun_
* Artistiques (8) : _Motus Domum, Percepto, 3 résidences R&D, Seeker, Mosaico, ULO_

Le projet SAV+R
=====
Simulation d’Acoustique Volumétrique en VR/MR

![audiodice -image-no-border](./img/dice-rond.png){width=20%}
![OSM -image-no-border](./img/OSM3-rond.png){width=20%}
![centech  -image-no-border](./img/interp-centech-rond.png){width=20%}

Concepts-clefs
==============

* Navigation en 6-ddl
* Réponse impulsionnelle (RI)
* Équiper les lieux plutôt que les individus

1 --- Capture audio pour navigation à 6-ddl
====

![mics -image-no-border](./img/savr_micro.jpg){width=40%}

Comment utiliser les microphones à 0-ddl et à 3-ddl afin d'optimiser la simulation acoustique à 6-ddl ?

Avancement --- Capture
====

![mic0 -image-no-border](./img/OSM2-octomic-ms.jpg){width=45%}
![mic3 -image-no-border](./img/OSM3-top-view.jpg){width=45%}

Capture des gestes des interprètes et capture acoustique en 3-ddl.

2 --- Simulation d'acoustique volumétrique
====

![simul -image-no-border](./img/savr_OSM_nav.jpg){width=40%}

Comment naviguer en 6-ddl dans un espace architectural dont l'acoustique est simulée ?

Avancement --- Simulation
====

![simulcheffe -image-no-border](./img/osm-livepose.jpg){width=45%}
![simulinterp -image-no-border](./img/interp-ms2.jpg){width=45%}

Amélioration de la navigation dans l'orcherstre et simulation d'ajout de source de son dans l'acoustique.

3 --- Espaces immersifs & rendus hétérogènes
====

Espaces immersifs & rendus hétérogènes
====

![satodice -image-no-border](./img/savr_rendering_sato_audiodice.jpg){width=40%}

Comment compenser la limitation des dispositifs de rendu ?

Avancement --- Espaces immersifs
====

![calibdice -image-no-border](./img/CIRMMT-audiodice-calib.jpg){width=30%}
![satodice -image-no-border](./img/surface-haptique-deformable.jpg){width=30%}

Calibrage des Audiodice et Prototype de surface haptique déformable.

Partenariats & outils
====

|                | SATIE | vaRays | Audiodice | Ambir |
|:--------------:|:-----:|:------:|:---------:|:-----:|
| Centech        |       |   x    |     x     |   x   |
| Core Sound     |   x   |        |           |   x   |
| Music6D        |   x   |        |     x     |       |
| OSM            |   x   |   x    |     x     |   x   |
| Résidences R&D |   x   |        |     x     |       |

Resultats sur 3 ans
====

* ✅ Nouveaux dispositifs sensoriels pour espace immersif, pour un groupe de personnes;
* ✅ Amélioration des algorithmes et des systèmes de rendu audio spatial;
* ✅ Amélioration de la perception des simulations immersives par les usagers;
* ✅ Nouveaux dispositifs de captation multipoints sonore et acoustique des lieux;
* ✅ Nouvelles expériences de navigation dans des scènes audio;
* ✅ nouvelles méthodes de prise en compte de la réponse acoustique dans la chaîne de reproduction
* ✅ Nouveaux types d'expériences musicales.

Partenaires sur 3 ans
=======

* Industriels (6): _Centech, Core Sound  🇺🇸, Expérience 6D, Atelier7hz, D-BOX, CAE_ 
* Académiques (2) : _McGill/IRCAM 🇫🇷 , CNRS/Pardoen 🇫🇷_
* Diffuseurs (1) : _OSM_
* Artistes (2) : _résidences R&D_

Merci pour votre participation !
================================
